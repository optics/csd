# csd

CSD based evaluation of metrology data with python

Content
=======
csd_calculation_example.py - 3 line example script with comments to show the basic mathematics
csd.py                     - definition of the csd object which can be imported or used directly with __main__ 
csd_user.py                - example script which imports csd.py

Prerequisites
=============
the script is written for python3 and uses the standard libraries numpy scipy and matplotlib

