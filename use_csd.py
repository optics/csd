#!/usr/bin/env python
'''
user script to apply the csd  
'''

__author__  = "Uwe Flechsig"
__contact__ = "uwe.flechsig@psi.ch"
__copyright = "2021"
__version__ = "0.1"

import numpy as np
import matplotlib.pyplot as plt
import csd

plt.close('all')           # close old plot windows
mycsd = csd.CSD()          # create a new csd object 
#mycsd.read('mydata.h5')   # read data from hdf5 file (structure is hardcoded) into object
mycsd.fill_example_data()  # fill example data as alternative to read
mycsd.print_statistics(height=True, raw=True)  # some output 
u1 = mycsd.fit_torus()                 # 2d fit to generate height error ue
mycsd.print_statistics(height=True)    # some output

mycsd.plot2dheight(raw=True)           # plot input height data (raw)
mycsd.plot2dheight(contour=False)      # plot height error

mycsd.slope_csd(height=True)           # calculate rms directly from height error
mycsd.plot_slope_rms(comment='(from 2d height error)')  # plot the rms
print("\n ==> total slope rms = {:.3f} murad".format(mycsd.srms[0]*1e6))  # the rms value we specified so far

plt.show()                             # show plots on screen
# end    
