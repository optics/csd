#!/usr/bin/env python
'''
csd based evaluation of height data

python example, this file contains the object definition which contains
all functionality, the code can be imported as python module or run directly 
in the __main__ section   
'''

__author__  = "Uwe Flechsig"
__contact__ = "uwe.flechsig@psi.ch"
__copyright = "2021"
__version__ = "0.1"

import numpy as np
import scipy.signal
import h5py
import matplotlib.pyplot as plt


# a 2d fit function from the web z= f(x,y)
def polyfit2d(x, y, z, kx=3, ky=3, order=None):
    '''
    Two dimensional polynomial fitting by least squares.
    Fits the functional form f(x,y) = z.

    Notes
    -----
    Resultant fit can be plotted with:
    np.polynomial.polynomial.polygrid2d(x, y, soln[0].reshape((kx+1, ky+1)))

    Parameters
    ----------
    x, y: array-like, 1d
        x and y coordinates.
    z: np.ndarray, 2d
        Surface to fit.
    kx, ky: int, default is 3
        Polynomial order in x and y, respectively.
    order: int or None, default is None
        If None, all coefficients up to maxiumum kx, ky, ie. up to and including x^kx*y^ky, are considered.
        If int, coefficients up to a maximum of kx+ky <= order are considered.

    Returns
    -------
    Return paramters from np.linalg.lstsq.

    soln: np.ndarray
        Array of polynomial coefficients.
    residuals: np.ndarray
    rank: int
    s: np.ndarray

    '''

    # grid coords
    x, y = np.meshgrid(x, y)
    # coefficient array, up to x^kx, y^ky
    coeffs = np.ones((kx+1, ky+1))

    # solve array
    a = np.zeros((coeffs.size, x.size))

    # for each coefficient produce array x^i, y^j
    for index, (i, j) in enumerate(np.ndindex(coeffs.shape)):
        # do not include powers greater than order
        if order is not None and i + j > order:
            arr = np.zeros_like(x)
        else:
            arr = coeffs[i, j] * x**i * y**j
        a[index] = arr.ravel()

    # do leastsq fitting and return leastsq result
    return np.linalg.lstsq(a.T, np.ravel(z), rcond=None)
# end polyfit2d 

# the object definition
class CSD(object):
    '''our CSD object- we put everything in this object definition'''
    # define the functions
    def __init__(self):
        '''the constructor'''
        # define the variables
        self.u = np.empty(0)     # the height u(l, w)
        self.ue = np.empty(0)    # the height error 
        self.w = np.empty(0)     # the long direction (columns)
        self.ws = np.empty(0)    # the long direction for slope (columns)
        self.l = np.empty(0)     # the short direction (rows)
        self.dudw = np.empty(0)  # slope du/dw
        self.fitpars = np.empty(0) # the fit parameter (rw, pitch, rl, roll, u0)
        self.srms = np.empty(0)  # slope rms
        self.sf = np.empty(0)    # slope frequency

    def fill_example_data(self) :
        '''generate example data
           
           toroidal mirror with rw = 499m and rl = 0.8m
           misalignment: pitch, roll, no yaw
           plus ripple
        '''
        rw = 499      # radius in w (long)
        rl = 1.8      # radius in l (short)
        pitch = 9e-4  # pitch
        roll = 2.4e-3 # roll
        aw1 = 11e-9   # amplitude in w
        aw2 = 15e-9   # amplitude in w
        al = 34e-9    # amplitude in l
        tw1 = 7e-3    # periode or error in w
        tw2 = 23e-3   # periode or error in w
        tl = 6e-3     # periode or error in l
        nw = 1001     # datapoints in w
        nl = 21       # datapoints in l
        self.w = np.linspace(-0.1, 0.1, nw)   # 200 mm long mirror, 1 mm sampling 
        self.l = np.linspace(-0.01, 0.01, nl) # 20 mm width
        self.u = np.zeros((nl, nw))           # define appropriate height array filled with 0.0

        for row in np.arange(nl) :            # fill array with loop for better understanding
            for col in np.arange(nw) :
                #self.u[row, col] = -4 + 2 * (self.w[col])**2 + 13 * 
                #self.l[row]**2 + 3 * (self.w[col]) + 6 * self.l[row]
                self.u[row, col] = (rw - np.sqrt(rw**2 - self.w[col]**2) +
                                    rl - np.sqrt(rl**2 - self.l[row]**2) +
                                    pitch * self.w[col] + roll * self.l[row] +
                                    aw1 * np.cos(2 * np.pi/ tw1 * self.w[col]) + 
                                    aw2 * np.cos(2 * np.pi/ tw2 * self.w[col]) + 
                                    al * np.cos(2 * np.pi/ tl * self.l[row])
                                    )
# end fill_example_data

    def fit_torus(self) :
        '''toroidal fit of height data

        the function update the vector fitpars and calculates the height error ue
        
        Returns:
            the fit model as np.array
        '''
        # map our variables to polyfit2d input
        x = self.w
        y = self.l
        z = self.u
    
        solt = polyfit2d(x, y, z, kx=2, ky=2, order=2)               # solution tuple
        (u0, roll, l2, pitch, tmp1, tmp2, w2, tmp3, tmp4) = solt[0]  # map parameters back

        myflat = 5e-10     # define a limit, calc radii, avoid div by zero error
        rw = 0.5 / myflat
        rl = 0.5 / myflat
        if np.abs(l2) > myflat :
            rl = 0.5/ l2
        if np.abs(w2) > myflat :
            rw = 0.5/ w2    
        print("debug: fitpars:" , solt[0])
        print("rw = {:.3f}m, pitch = {:.4f}rad, rl = {:.3f}m, roll = {:.4f}rad, u0 = {:.3f}m".
              format(rw, pitch, rl, roll, u0))
        self.fitpars= np.array([rw, pitch, rl, roll, u0])  # update fitpars
        # u1= np.polynomial.polynomial.polygrid2d(x y, solt[0].reshape((3, 3)))
        # plt.matshow(z1)
        u1 = np.zeros((self.l.size, self.w.size)) 
        for row in np.arange(self.l.size) :            # fill array with loop
            for col in np.arange(self.w.size) :
                u1[row, col] = (rw - np.sqrt(rw**2 - self.w[col]**2) +
                                rl - np.sqrt(rl**2 - self.l[row]**2) +
                                pitch * self.w[col] + roll * self.l[row] + u0)

        self.ue = self.u - u1                     
        return u1
#end fit_torus

    def height2wslope(self, raw=False) :
        '''calculate slope in w from height data
        !!! the dimension in w shrinks by one (out[n] = a[n+1] - a[n])
        '''
        if raw :
            self.dudw = np.diff(self.u) / (self.w[1]- self.w[0])
        else :
            self.dudw = np.diff(self.ue) / (self.w[1]- self.w[0])
        self.ws = self.w[0:-1]  # one element shorter
# end height2wslope

    def plot2dheight(self, raw=False, contour=True) :
        '''plot 2d height or height error with/without contour lines
         
           Args:
               contour=True (bool) : with contour lines
               raw=False (bool) : raw height or height error
        '''
        plt.figure()
        if raw :
            uu = self.u
            zlabel = 'u (mm)'
            zscale = 1e3
            title = 'height'
        else :
            uu = self.ue
            zlabel = 'u (nm)'
            zscale = 1e9
            title = 'height error'

        if contour :    
            plt.contour(self.w*1e3, self.l*1e3, uu*zscale, 15, linewidths= 0.5, colors= 'k')  # contour lines
        plt.pcolormesh(self.w*1e3, self.l*1e3, uu*zscale, cmap=plt.get_cmap('rainbow'),
                       vmin=np.min(uu)*zscale, vmax=np.max(uu)*zscale, shading='auto')  
        cbar= plt.colorbar()
        cbar.set_label(zlabel) #, rotation=270)
        plt.xlabel('w (mm)')
        plt.ylabel('l (mm)')
        plt.title(title)    
# end plot2dheight

    def plot2dslope(self) :
        plt.figure()  # new plot window
        #cp= plt.contour(self.ws * 1e3, self.l * 1e3, self.dudw * 1e6, 15, linewidths = 0.5, colors= 'k')  # contour lines
        plt.pcolormesh(self.ws * 1e3, self.l * 1e3, self.dudw * 1e6, cmap=plt.get_cmap('rainbow'),
                       vmin=np.min(self.dudw) * 1e6, vmax=np.max(self.dudw) * 1e6, shading='auto')  
        cbar = plt.colorbar()
        zlabel = 'dudw ($\mu$rad)'
        cbar.set_label(zlabel) #, rotation=270)
        plt.xlabel('w (mm)')
        plt.ylabel('l (mm)')
        plt.title('slope w')
# end plot2dslope 

    def plot_slope_rms(self, comment='') :
        plt.figure()  # new plot window
        plt.plot(self.sf, self.srms*1e6)
        plt.xlabel('spatial frequency (1/m)')
        plt.ylabel('rms ($\mu$rad)')
        plt.semilogx()
        plt.title('result: average slope rms in w {}'.format(comment))
# end plot_slope_rms 

    def print_statistics(self, height = True, raw = False) :
        '''print statistics

        Args:
            height = True (bool) : height statistics or slope
            raw = False (bool) : raw height, default is height error
        '''
        
        if height :
            if raw :
                print("height: rms= {:.3f} mum, pv= {:.3f} mum".
                      format(np.std(self.u) * 1e6,  
                             (np.amax(self.u) - np.amin(self.u)) * 1e6))
            else:
                print("height_error: rms= {:.3f} mum, pv= {:.3f} mum".
                      format(np.std(self.ue) * 1e6,  
                             (np.amax(self.ue) - np.amin(self.ue)) * 1e6))
        else:
                  print("slope: rms= {:.3f} murad, pv= {:.3f} murad".
                        format(np.std(self.dudw) * 1e6, 
                               (np.amax(self.dudw) - np.amin(self.dudw)) * 1e6))
# end print_statistics    

    def read(self, h5filename = 'mydata.h5') :
        '''read data from hdf5 file (without error handling) into object

        Args:
            h5filename (str) : filename of data file default: 'mydata.h5'

        Example:
            >>> mycsd = CSD()
                mycsd.read('mynewdata.h5')
           
        '''
        h5f = h5py.File(h5filename, 'r')  # open file handle
        dpath = '/height/raw/'            # data path in hdf5 file (hardcoded here)
        dpath = '/psi/OL_PC_0040.mda/face up/rawdata/'  # overwrite it here
        self.u = h5f[dpath + "slope"][:]     # read the matrix u
        self.w = h5f[dpath + "posx"][:]      # read vector w
        l = h5f[dpath + "posy"][:]      # read vector l
        self.l = l[:,0]  
        h5f.close()  # close file handle
# end read function     

    def slope_csd(self, height=True) :
        '''calculates the slope csd from height error or slope error
           
           steps: (1) 1d  PSD for each line in l - psd(l, w)
                  (2) average psds - we end with a 1d psd(w) 
                  (3) calculate 1d slope psd
                  (4) calc csd 
           Args:
               height=True (bool): calculate from height or slope
        '''

        wf = 1.0 / np.abs(self.w[1] - self.w[0])  # sampling rate in w
        if height :
            (hf, h2psd) = scipy.signal.periodogram(self.ue, wf, window='blackman', 
                                                   return_onesided=True)  # one sided psd with taper
            h1psd = np.mean(h2psd, axis=0)       # average over l
            s1psd = h1psd * (2 * np.pi * hf)**2  # height to slope in fourier space
            self.sf = hf
        else :
            (sf, s2psd) = scipy.signal.periodogram(self.dudw, wf, window='blackman', 
                                                   return_onesided=True)  # one sided psd with taper
            s1psd = np.mean(s2psd, axis=0)    # average over l
            self.sf = sf 
        s1csd = np.cumsum(s1psd[::-1])[::-1] * (self.sf[1] - self.sf[0])
        self.srms = np.sqrt(s1csd)
#end slope_csd
# end object definition

# the main routine
if __name__ == '__main__' :
    plt.close('all')           # close old plot windows
    mycsd = CSD()              # create a new csd object 
    #mycsd.read('mydata.h5')   # read data from hdf5 file (structure is hardcoded) into object
    mycsd.fill_example_data()  # fill example data as alternative to reas
    mycsd.print_statistics(height=True, raw=True)  # some output 
    u1 = mycsd.fit_torus()     # 2d fit to generate height error ue
    mycsd.print_statistics(height=True)  # some output

    mycsd.plot2dheight(raw=True)         # plot input height data (raw)
    mycsd.plot2dheight(contour=False)    # plot height error
 
    mycsd.slope_csd(height=True)         # calculate rms directly from height error
    mycsd.plot_slope_rms(comment='(from 2d height error)')  # plot the rms
    print("\n ==> total slope rms = {:.3f} murad".format(mycsd.srms[0]*1e6))  # the rms value we specified so far

    debug = False
    if debug :                           # the alternative way (just for rough cross check)
        mycsd.height2wslope(raw=False)         # height error to slope, the w axis shinks by 1 point
        mycsd.print_statistics(height=False)   # some output
        mycsd.plot2dslope()                    # plot intermediate slope error
        mycsd.slope_csd(height=False)          # calc csd from slope error
        mycsd.plot_slope_rms(comment='(from 2d slope error)')  # plot the rms
    
    plt.show()  # show plots on screen
# end    
