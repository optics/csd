#!/usr/bin/env python
'''
3 line example code to demonstrate the CSD calculation with python

we assume to start with two 1d vectors x, y; 
for example: y the slope measured with an LTP as function of the position x; 
we assume: x is sampled equidistant; 
some math details: we calculate the one sided real (not complex) psd with blackman taper, 
integrate it from high to low frequencies and take the square root out of it to end up 
with the rms contribution as function of spatial frequency, the value at the lowest 
frequency is the total rms    

Args:
        x (numpy.array): x - vector 
        y (numpy.array): y - vector 
        
        
    Returns:
        f (numpy.array): spatial frequency
        rms (numpy.array): rms contribution

'''

import numpy 
import scipy.signal

# (0) we assume we already have the vectors x and y of measured data

# (1) we calculate the sampling frequency, taking the first two points (we assume equidistant sampling);
#     we apply abs() to be always positive independent of sorting
sf = 1.0 / numpy.abs(x[1] - x[0])

# (2) we calculate the frequency and psd vectors (one sided, real psd or peroidogram, with blackman taper)
(f, psd) = scipy.signal.periodogram(y, sf, window='blackman', return_onesided=True)

# (3) rms contribution as function of frequency; integrated from high to low frequencies
#     details: 3a) we reverse the psd vector; 3b) running sum of the reversed vector
#              3c) reverse again; 3d) multiply by Delta f to make the integration - have the CSD
#              3e) take the square root of CSD to show the rms contribution 
rms = numpy.sqrt(numpy.cumsum(psd[::-1])[::-1] * (f[1] - f[0]))  

# (4) we have the result, the two vectors f and rms
#     print("f: ", f)
#     print("rms: ", rms)
